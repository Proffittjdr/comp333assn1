Timer();

var timer = setInterval(function() { Timer() }, 3000);

document.getElementById("chatform")
    .addEventListener("keyup", function(event) {
        event.preventDefault();
        if (event.keyCode === 13) {
            document.getElementById("chatbutt").click();
        }
    });

function openreg() {
    document.getElementById("popuplogin").style.visibility = "hidden";
    document.getElementById("popupRegister").style.visibility = "visible";
}

function openlogin() {
    document.getElementById("popupRegister").style.visibility = "hidden";
    document.getElementById("popuplogin").style.visibility = "visible";
}

function Timer() {
    GetChat();
    GetUsers();
    Query("Update", "", "", "");
}

function Login() {
    Query("Login", document.getElementById("loginuser").value, document.getElementById("loginpass").value, "");
}

function Logout() {
    Query("Logout", "", "", "");

}

function Register() {
    if (document.getElementById("regpass").value == document.getElementById("confpass").value && document.getElementById("regpass").value != "") {
        Query("Reg", document.getElementById("reguser").value, document.getElementById("regpass").value, "");
    } else {
        alert("Passwords don't match or are invalid!");
    }
}

function Message() {
    Query("Message", "", "", document.getElementById("chatform").value);
}

function GetChat() {
    Query("Chat", "", "", "", "");
}

function DisplayChat(json) {
    var data = JSON.parse(json);
    data.reverse();
    for (var i = 0; i < data.length; i++) {
        var row = data[i];
        if (row.Message != "") {
            document.getElementById("Chat").innerHTML += "<div class='chatdiv'><b>" + row.User + ":</b>  " + row.Message + "</div>";
        }
    }
}

function ClearChat() {
    document.getElementById("Chat").innerHTML = "";
}

function GetUsers() {
    Query("Online", "", "", "");
    Query("Offline", "", "", "");
}

function DisplayUsers(json, status) {
    var data = JSON.parse(json);
    for (var i = 0; i < data.length; i++) {
        var row = data[i];
        var ul = document.getElementById("Userlist" + status);
        var li = document.createElement("li");
        ul.appendChild(li);
        li.appendChild(document.createTextNode(row.User));
    }
}

function ClearUsers(status) {
    document.getElementById("Userlist" + status).innerHTML = "";
}

function Query(type, user, pass, message) {
    var send = "type=" + JSON.stringify(type);

    if (type == "Reg" || type == "Login") {
        send += "&pass=" + JSON.stringify(pass) + "&user=" + JSON.stringify(user);
    }
    if (type == "Message") {
        send += "&message=" + JSON.stringify(message);
    }


    var xhr = new XMLHttpRequest();
    xhr.onload = function() {
        if (this.status == 200) {
            var json = xhr.responseText;

            if (json == "1") {
                if (type == "Reg") {
                    alert("Registered as " + user);
                    document.getElementById("popupRegister").style.visibilty = "hidden";
                } else if (type == "Login") {
                    alert("Welcome " + user);
                    Query("Update", "", "", "");
                    document.getElementById("popuplogin").style.visibility = "hidden";
                } else if (type == "Update") {
                    GetUsers();
                } else if (type == "Message") {
                    document.getElementById("chatform").value = "";
                    GetChat();
                } else if (type == "Logout") {
                    console.log("bye!");
                    document.getElementById("popuplogin").style.visibility = "visible";
                }

            } else {
                if (type == "Reg") {
                    alert("Please try another username.");
                } else if (type == "Login") {
                    alert("Incorrect Username or Password.");
                } else if (type == "Update") {
                    console.log("User update failed.");
                } else if (type == "Message") {
                    alert("Chat Error! Please try again. ");
                } else if (type == "Logout") {
                    console.log("here to stay?");
                }
            }

            if (json != [] || json != "[]") {
                if (type == "Chat") {
                    ClearChat();
                    DisplayChat(json);
                } else if (type == "Online") {
                    ClearUsers("on");
                    DisplayUsers(json, "on");
                } else if (type == "Offline") {
                    ClearUsers("off");
                    DisplayUsers(json, "off");
                }
            } else {
                if (type == "Chat") {
                    console.log("Failed to gather chat");
                } else if (type == "Online") {
                    console.log("Failed to gather online users");
                } else if (type == "Offline") {
                    console.log("Failed to gather offline users");
                }
            }
        }

    };

    xhr.open("POST", "functions/query.php", true);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send(send);
}