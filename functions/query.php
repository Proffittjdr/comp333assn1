<?php
date_default_timezone_set("Pacific/Auckland");
try
{ 
    session_start();
    $type = json_decode($_POST["type"], false);

    //USER
    if ($type == "Login" || $type == "Reg") 
    {
        $user = json_decode($_POST["user"], false);
        $pass = json_decode($_POST["pass"], false);
    }
    else if ($type == "Message" || $type == "Update" || $type == "Logout") 
    {
        if (isset($_SESSION['User']))
        {
            $user = $_SESSION['User'];
            
        }
    }

    //TIME
    if ($type == "Update" || $type == "Message" || $type == "Reg")
    {
        $date = date('Y-m-d H:i:s');
    }
    else if ($type == "Online" || $type == "Offline" || $type == "Logout")
    {
        $time = strtotime('-5 minutes');
        $date = date('Y-m-d H:i:s', $time);
    }

    //SQL
    if ($type == "Login") {
        $sql = "Select * from Users where User = '$user' and Pass = '$pass'";
    }
    else if ($type == "Reg") {
        $sql = "INSERT INTO Users(`User`, `Pass`, `Time`) VALUES ('$user', '$pass', '$date')";
    }
    else if ($type == "Online") {
        $sql = "Select * from Users where Time >= '$date'";
    }
    else if ($type == "Offline") {
        $sql = "Select * from Users where Time < '$date'";
    }
    else if ($type == "Chat") {
        $date = date('Y-m-d');
        $sql = "Select * from Messages WHERE DATE(date) = '$date'";
    }
    else if ($type == "Update" ||$type == "Logout") 
    {
        $sql = "UPDATE Users SET Time = '$date' WHERE user = '$user'";
    }
    else if ($type == "Message") {
        $message = json_decode($_POST["message"], false);
        $sql = "INSERT INTO Messages(`User`, `Message`, `Date`) VALUES ('$user', '$message', '$date')";
    }

    //$conn = new PDO('mysql:host=mysql.cms.waikato.ac.nz;dbname=jdrp1', 'jdrp1', 'my10873675sql');
    $conn = new PDO('mysql:host=localhost:8889; dbname=jdrp1;', 'root', 'root');
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    $stmt = $conn->prepare($sql);
    $stmt->execute();

    if ($type == "Chat" || $type == "Online" || $type == "Offline") 
    {
        $result = $stmt->fetchAll();
        $return = array();
        foreach($result as $row) 
        {
            $return[] = $row;
        }
    }
    else 
    {
        $return = $stmt->rowCount();
        if ($type == "Login" & $return > 0 ) 
        {
            $_SESSION['User']= $user;
        }
    }

    header('Content-type: application/json');
    echo json_encode($return);
}
catch(PDOException $exception)
{ 
    return $exception; 
} 

